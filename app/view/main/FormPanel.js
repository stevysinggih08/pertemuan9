Ext.define('Pertemuan9.view.main.FormPanel', {
    extend: 'Ext.form.Panel',

    shadow: true,
    xtype: 'editform',
    id: 'editform',
    items: [
        {
            xtype: 'textfield',
            id: 'myname',
            name: 'name',
            label: 'Name',
            placeHolder: 'Your Name',
            autoCapitalize: true,
            required: true,
            clearIcon: true
        },
        {
            xtype: 'emailfield',
            id: 'myemail',
            name: 'email',
            label: 'Email',
            placeHolder: 'me@sencha.com',
            clearIcon: true
        },
        {
            xtype: 'textfield',
            id: 'myphone',
            name: 'phone',
            label: 'Phone',
            placeHolder: '081455565758',
            clearIcon: true
        },
        {
            xtype: 'button',
            ui: 'action',
            text: 'Simpan Perubahan',
            handler: 'onSimpanPerubahan'
        },
        {
            xtype: 'button',
            ui: 'confirm',
            text: 'Tambah Personnel',
            handler: 'onTambahPersonnel'
        }
    ]

});